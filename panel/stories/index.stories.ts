import { Icon } from '~/icon';
import Panel from './Panel.stories.svelte';
import PanelImage from './PanelImage.stories.svelte';

export const defaultPanel = () => Panel;
export const imagePanel = () => PanelImage;
