import { Button } from '~/button';
import ButtonCustom from './button.stories.svelte';

export const storyButton = () => Button;

export const buttonPrimary = () => ({
  Component: Button,
  props: {
    primary: true,
  },
});

export const buttonSecondary = () => ({
  Component: Button,
  props: {
    secondary: true,
  },
});

export const buttonDisabled = () => ({
  Component: Button,
  props: {
    disabled: true,
  },
});

export const buttonCustom = () => ButtonCustom;
