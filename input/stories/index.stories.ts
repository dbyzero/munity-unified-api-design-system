import { Input } from '~/input';
import InputDisabled from './input.disabled.svelte';
import InputCustomPlaceholder from './input.placeholder.svelte';

export const basicInput = () => Input;
export const disabledInput = () => InputDisabled;
export const customPlaceholderInput = () => InputCustomPlaceholder;
